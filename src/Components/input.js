import React, {Component} from 'react';
import './input.css'

export class Input extends Component{
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.setParam(event.target.value);
    }

    render() {
        return(
            <input type={"text"}
                   className={'input'}
                   value={this.props.inputValue}
                   onChange={this.handleChange}/>
        )
    }
}