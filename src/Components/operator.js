import React, {Component} from 'react';
import './operator.css'

export class Operator extends Component{
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {
        const { opType } = this.props;
        this.props.shouldDo(opType);
    }

    render() {
        return(
            <button className={'orangeButton'} onClick={this.handleClick}>{this.props.opType}</button>
        )
    }
}