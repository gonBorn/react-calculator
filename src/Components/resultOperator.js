import React, {Component} from 'react';
import './resultOperator.css'

export class ResultOperator extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <button className={'blueButton'}>{this.props.content}</button>
        )
    }
}