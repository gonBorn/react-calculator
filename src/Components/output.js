import React, {Component} from 'react';
import './output.css'

export class Output extends Component{
    constructor(props) {
        super(props);
        this.state = {
            res : ''
        }
    }

    render() {
        return(
            <div className={'output'}>{this.props.content}</div>
        )
    }
}