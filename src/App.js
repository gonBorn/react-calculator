import React, {Component} from 'react'
import './App.css';
import {Operator} from "./Components/operator";
import {ResultOperator} from "./Components/resultOperator";
import {Input} from "./Components/input";
import {Output} from "./Components/output";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            operator : '',
            param1 : '',
            param2 : '',
            result : ''
        }
        this.setParam1 = this.setParam1.bind(this);
        this.setParam2 = this.setParam2.bind(this);
        // this.handleAddButton = this.handleAddButton.bind(this);
        // this.handleMinusButton = this.handleMinusButton.bind(this);
        // this.handleMultiplyButton = this.handleMultiplyButton.bind(this);
        // this.handleDivideButton = this.handleDivideButton.bind(this);
        this.handleEqual = this.handleEqual.bind(this);
        this.handleClear = this.handleClear.bind(this);
        //this.handleMinus = this.handleMinus.bind(this);
    }

    setParam1(value) {
        this.setState({
            param1 : value
        })
    }

    setParam2(value) {
        this.setState({
            param2: value
        });
    }

    // handleAddButton() {
    //     this.setState({
    //         operator: '+'
    //     });
    // }
    // handleMinusButton() {
    //     this.setState({
    //         operator: '-'
    //     });
    // }
    // handleMultiplyButton() {
    //     this.setState({
    //         operator: 'x'
    //     });
    // }
    // handleDivideButton() {
    //     this.setState({
    //         operator: '/'
    //     });
    // }

    handleEqual() {
        const {operator, param1, param2} = this.state;

        if (operator === '+') {
            this.setState({
                result : Number(param1)+Number(param2)
            }) ;
            return;
        }

        if(operator === '-') {
            this.setState({
                result : Number(param1) - Number(param2)
            }) ;
            return;
        }

        if(operator === 'x') {
            this.setState({
                result : Number(param1) * Number(param2)
            }) ;
            return;
        }

        if(operator === '/') {
            this.setState({
                result : Number(param1) / Number(param2)
            }) ;

        }

    }
    handleClear() {
        this.setState({
            operator : '',
            param1 : '',
            param2 : '',
            result : ''
        })
    }

    handleOperate(value) {
        this.setState({
            operator: value
        });
    }

    render() {

        console.log('=======', this.state);
        return(
            <div className="App">
                <div className={'wrapper'}>
                    <Output content={this.state.result} />
                    {/*<button className={'orangeButton'} onClick={this.handleAddButton}>+</button>*/}
                    {/*<button className={'orangeButton'} onClick={this.handleMinusButton}>-</button>*/}
                    {/*<button className={'orangeButton'} onClick={this.handleMultiplyButton}>x</button>*/}
                    {/*<button className={'orangeButton'} onClick={this.handleDivideButton}>/</button>*/}
                    <Operator shouldDo={this.handleOperate.bind(this)} opType={"+"}/>
                    <Operator shouldDo={this.handleOperate.bind(this)} opType={"-"}/>
                    <Operator shouldDo={this.handleOperate.bind(this)} opType={"*"}/>
                    <Operator shouldDo={this.handleOperate.bind(this)} opType={"/"}/>
                    {/*<Operator content={"-"} handleClick={this.handleMinus}/>*/}
                    {/*<ResultOperator content={"AC"} onClick={this.handlePlus}/>*/}
                    {/*<ResultOperator content={"="} onClick={this.handlePlus}/>*/}
                    <button className={'blueButton'} onClick={this.handleEqual}>=</button>
                    <button className={'blueButton'} onClick={this.handleClear}>AC</button>
                    <Input setParam={this.setParam1} inputValue={this.state.param1}/>
                    <Input setParam={this.setParam2} inputValue={this.state.param2}/>
                </div>
            </div>
        )
    };


}

export default App;
